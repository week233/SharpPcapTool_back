﻿namespace SharpPcapTool
{
    partial class MainForm
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbDeviceList = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lsbIPMap = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txbGetwayIP = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txbGetwayMAC = new System.Windows.Forms.TextBox();
            this.btnArp = new System.Windows.Forms.Button();
            this.txbLocalMAC = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txbLocalIP = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.btnScan = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txbStartIP = new System.Windows.Forms.TextBox();
            this.txbEndIP = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.btnArpGetway = new System.Windows.Forms.Button();
            this.btnArpStorm = new System.Windows.Forms.Button();
            this.contextMenuStrip = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.menuRead = new System.Windows.Forms.ToolStripMenuItem();
            this.menuSave = new System.Windows.Forms.ToolStripMenuItem();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.contextMenuStrip.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmbDeviceList
            // 
            this.cmbDeviceList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDeviceList.FormattingEnabled = true;
            this.cmbDeviceList.Location = new System.Drawing.Point(67, 24);
            this.cmbDeviceList.Name = "cmbDeviceList";
            this.cmbDeviceList.Size = new System.Drawing.Size(310, 20);
            this.cmbDeviceList.TabIndex = 0;
            this.cmbDeviceList.SelectedIndexChanged += new System.EventHandler(this.cmbDeviceList_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "设备：";
            // 
            // lsbIPMap
            // 
            this.lsbIPMap.ContextMenuStrip = this.contextMenuStrip;
            this.lsbIPMap.FormattingEnabled = true;
            this.lsbIPMap.ItemHeight = 12;
            this.lsbIPMap.Location = new System.Drawing.Point(6, 58);
            this.lsbIPMap.Name = "lsbIPMap";
            this.lsbIPMap.Size = new System.Drawing.Size(274, 208);
            this.lsbIPMap.TabIndex = 3;
            this.lsbIPMap.SelectedIndexChanged += new System.EventHandler(this.lsbIPMap_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 12);
            this.label2.TabIndex = 4;
            this.label2.Text = "网关IP：";
            // 
            // txbGetwayIP
            // 
            this.txbGetwayIP.Enabled = false;
            this.txbGetwayIP.Location = new System.Drawing.Point(67, 64);
            this.txbGetwayIP.Name = "txbGetwayIP";
            this.txbGetwayIP.Size = new System.Drawing.Size(100, 21);
            this.txbGetwayIP.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(194, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 6;
            this.label3.Text = "网关MAC：";
            // 
            // txbGetwayMAC
            // 
            this.txbGetwayMAC.Enabled = false;
            this.txbGetwayMAC.Location = new System.Drawing.Point(268, 64);
            this.txbGetwayMAC.Name = "txbGetwayMAC";
            this.txbGetwayMAC.Size = new System.Drawing.Size(100, 21);
            this.txbGetwayMAC.TabIndex = 7;
            // 
            // btnArp
            // 
            this.btnArp.Enabled = false;
            this.btnArp.Location = new System.Drawing.Point(293, 99);
            this.btnArp.Name = "btnArp";
            this.btnArp.Size = new System.Drawing.Size(86, 23);
            this.btnArp.TabIndex = 8;
            this.btnArp.Text = "ARP主机欺骗";
            this.btnArp.UseVisualStyleBackColor = true;
            this.btnArp.Click += new System.EventHandler(this.btnArp_Click);
            // 
            // txbLocalMAC
            // 
            this.txbLocalMAC.Enabled = false;
            this.txbLocalMAC.Location = new System.Drawing.Point(268, 102);
            this.txbLocalMAC.Name = "txbLocalMAC";
            this.txbLocalMAC.Size = new System.Drawing.Size(100, 21);
            this.txbLocalMAC.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(194, 105);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 11;
            this.label4.Text = "本地MAC：";
            // 
            // txbLocalIP
            // 
            this.txbLocalIP.Enabled = false;
            this.txbLocalIP.Location = new System.Drawing.Point(67, 102);
            this.txbLocalIP.Name = "txbLocalIP";
            this.txbLocalIP.Size = new System.Drawing.Size(100, 21);
            this.txbLocalIP.TabIndex = 10;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(53, 12);
            this.label5.TabIndex = 9;
            this.label5.Text = "本地IP：";
            // 
            // btnScan
            // 
            this.btnScan.Location = new System.Drawing.Point(293, 58);
            this.btnScan.Name = "btnScan";
            this.btnScan.Size = new System.Drawing.Size(84, 23);
            this.btnScan.TabIndex = 14;
            this.btnScan.Text = "扫描";
            this.btnScan.UseVisualStyleBackColor = true;
            this.btnScan.Click += new System.EventHandler(this.btnScan_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 26);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(53, 12);
            this.label7.TabIndex = 15;
            this.label7.Text = "起始IP：";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(196, 26);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(53, 12);
            this.label8.TabIndex = 16;
            this.label8.Text = "终止IP：";
            // 
            // txbStartIP
            // 
            this.txbStartIP.Location = new System.Drawing.Point(69, 21);
            this.txbStartIP.Name = "txbStartIP";
            this.txbStartIP.Size = new System.Drawing.Size(100, 21);
            this.txbStartIP.TabIndex = 17;
            // 
            // txbEndIP
            // 
            this.txbEndIP.Location = new System.Drawing.Point(268, 21);
            this.txbEndIP.Name = "txbEndIP";
            this.txbEndIP.Size = new System.Drawing.Size(100, 21);
            this.txbEndIP.TabIndex = 18;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.cmbDeviceList);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txbGetwayIP);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.txbGetwayMAC);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.txbLocalMAC);
            this.groupBox1.Controls.Add(this.txbLocalIP);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Location = new System.Drawing.Point(11, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 137);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "本地网络";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.btnArpStorm);
            this.groupBox2.Controls.Add(this.btnArpGetway);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.btnArp);
            this.groupBox2.Controls.Add(this.btnScan);
            this.groupBox2.Controls.Add(this.txbEndIP);
            this.groupBox2.Controls.Add(this.txbStartIP);
            this.groupBox2.Controls.Add(this.lsbIPMap);
            this.groupBox2.Location = new System.Drawing.Point(11, 155);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(385, 273);
            this.groupBox2.TabIndex = 20;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "局域网IP与MAC映射：";
            // 
            // btnArpGetway
            // 
            this.btnArpGetway.Enabled = false;
            this.btnArpGetway.Location = new System.Drawing.Point(293, 142);
            this.btnArpGetway.Name = "btnArpGetway";
            this.btnArpGetway.Size = new System.Drawing.Size(86, 23);
            this.btnArpGetway.TabIndex = 19;
            this.btnArpGetway.Text = "ARP网关欺骗";
            this.btnArpGetway.UseVisualStyleBackColor = true;
            this.btnArpGetway.Click += new System.EventHandler(this.btnArpGetway_Click);
            // 
            // btnArpStorm
            // 
            this.btnArpStorm.Location = new System.Drawing.Point(293, 185);
            this.btnArpStorm.Name = "btnArpStorm";
            this.btnArpStorm.Size = new System.Drawing.Size(86, 23);
            this.btnArpStorm.TabIndex = 20;
            this.btnArpStorm.Text = "ARP风暴";
            this.btnArpStorm.UseVisualStyleBackColor = true;
            this.btnArpStorm.Click += new System.EventHandler(this.btnArpStorm_Click);
            // 
            // contextMenuStrip
            // 
            this.contextMenuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.menuRead,
            this.menuSave});
            this.contextMenuStrip.Name = "contextMenuStrip";
            this.contextMenuStrip.Size = new System.Drawing.Size(153, 70);
            // 
            // menuRead
            // 
            this.menuRead.Name = "menuRead";
            this.menuRead.Size = new System.Drawing.Size(152, 22);
            this.menuRead.Text = "读取映射表(&L)";
            this.menuRead.Click += new System.EventHandler(this.menuRead_Click);
            // 
            // menuSave
            // 
            this.menuSave.Name = "menuSave";
            this.menuSave.Size = new System.Drawing.Size(152, 22);
            this.menuSave.Text = "保存映射表(&S)";
            this.menuSave.Click += new System.EventHandler(this.menuSave_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(406, 435);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "ARP欺骗工具";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.contextMenuStrip.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ComboBox cmbDeviceList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lsbIPMap;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txbGetwayIP;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txbGetwayMAC;
        private System.Windows.Forms.Button btnArp;
        private System.Windows.Forms.TextBox txbLocalMAC;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txbLocalIP;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button btnScan;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txbStartIP;
        private System.Windows.Forms.TextBox txbEndIP;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button btnArpGetway;
        private System.Windows.Forms.Button btnArpStorm;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip;
        private System.Windows.Forms.ToolStripMenuItem menuRead;
        private System.Windows.Forms.ToolStripMenuItem menuSave;
    }
}

